﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnControllerColliderHit (ControllerColliderHit hit) {
		if(hit.gameObject.CompareTag("ObjectToRecolect")){
			print (hit.gameObject.name);
			hit.gameObject.GetComponent<ObjectToRecolect>().ObjectTouched();
		}
		
		if(hit.gameObject.name=="Final"){
			print (hit.gameObject.name);
			hit.gameObject.GetComponent<Final>().ObjectTouched();
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour {

	public int id;
	public Texture _textDef;
	public Texture _textOver;
	
	void OnMouseDown () {
		switch(id){
			case 1:
			 	Application.LoadLevel(1);
			
			break;
			case 2:
			 	Application.LoadLevel(0);
			
			break;
			default:
			break;
		}
	}
	
	void OnMouseOver () {
		if(_textOver)
			guiTexture.texture = _textOver;
	}
	
	void OnMouseExit () {
		if(_textOver)
			guiTexture.texture = _textDef;

	}

}
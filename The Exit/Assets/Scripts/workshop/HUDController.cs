﻿using UnityEngine;
using System.Collections;

public class HUDController : MonoBehaviour {
	public GUITexture[] items;
	public Texture activeItem;
	public GameObject CountDownTimer;
	public float TimeDown=0.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void ItemActive(int index){
		items[index].texture=activeItem;
	}
	
	public void InitClock(){
		CountDownTimer.gameObject.GetComponent<CountDownTimer>().StartTimer(TimeDown);
	}
	
	public void StopClock(){
		CountDownTimer.gameObject.GetComponent<CountDownTimer>().StopTimer();
	}
}
